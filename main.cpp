#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <array>
#include <vector>
#include <map>
#include "inc/emp.h"

using namespace std;

int main() {
	ifstream list("../Employes.dat");
	if (!list) {
		cout << "No file!";
	}
	else {
		vector<Employe> employes;
		for (string buff; list >> buff;) {
			string temp;
			array<string, 3> line;
			int c = 0;
			for (int i = 0; i < buff.size(); i++) {
				if (buff[i] == ';') {
					line[c] = temp;
					c++;
					temp.clear();
				}
				else {
					temp += buff[i];
				}
			}
			Employe n(line[0], line[1], atoi(line[2].c_str()));
			employes.push_back(n);
		}
		string pos;
		cin >> pos;
		for (auto employe : employes) {
			if (employe.getPosition() == pos) {
				cout << setw(10) << left << employe.getName() << " " << employe.getSalary() << endl;
			}
		}
	}
}