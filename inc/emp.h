#pragma once
using namespace std;

class Employe {
	string position;
	string name;
	int work_time;
	int get�oefficient() {
		map<string, int> positions = { {"director", 500}, {"assistant", 200}, {"programmer", 350} };
		return positions[position];
	}
public:
	Employe(string pos, string nm, int wk) {
		position = pos;
		name = nm;
		work_time = wk;
	}
	string getPosition() {
		return position;
	}
	string getName() {
		return name;
	}
	int getSalary() {
		return (work_time * get�oefficient());
	}
};